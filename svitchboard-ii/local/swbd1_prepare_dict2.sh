#!/bin/bash

# Formatting the Mississippi State dictionary for use in Edinburgh. Differs 
# from the one in Kaldi s5 recipe in that it uses lower-case --Arnab (Jan 2013)

# To be run from one directory above this script.

. path.sh

#check existing directories
[ $# != 0 ] && echo "Usage: local/swbd1_data_prep.sh" && exit 1;

# srcdir=data/local/train  # This is where we downloaded some stuff..
dir=data/local/dict
mkdir -p $dir
#srclexicon=/n/key050/s1/yzliu/asr/recipe/swbd/s5b/data/local/dict/lexicon.txt 
srclexicon=swbd-data/lexicon.txt

# srcdict=$srcdir/sw-ms98-dict.text

# assume swbd_p1_data_prep.sh was done already.
[ ! -f "$srclexicon" ] && echo "No such file $srclexicon" && exit 1;

#(2a) Dictionary preparation:
# Get dictionary for vocalbulary defined in the subset
python /g/ssli/transitory/yzliu/project/svb/svb/get-groundset.py -d data/local/train/word.id -i ${srclexicon} -o $dir/lexicon1.txt 



cat $dir/lexicon1.txt | awk '{ for(n=2;n<=NF;n++){ phones[$n] = 1; }} END{for (p in phones) print p;}' | \
  grep -v sil > $dir/nonsilence_phones.txt  || exit 1;

( echo sil; ) > $dir/silence_phones.txt
# ( echo sil; echo spn ) > $dir/silence_phones.txt

echo sil > $dir/optional_silence.txt

# No "extra questions" in the input to this setup, as we don't
# have stress or tone.
echo -n >$dir/extra_questions.txt

# Add to the lexicon the silences, noises etc.
#( echo '!sil sil'; echo '<unk> spn' ) \
( echo '!sil sil'; ) \
  | cat - $dir/lexicon1.txt  > $dir/lexicon2.txt || exit 1;

pushd $dir >&/dev/null
ln -sf lexicon2.txt lexicon.txt # This is the final lexicon.
popd >&/dev/null

# local/swbd1_map_words.pl -f 1 $dir/lexicon2.txt | sort -u \
#   > $dir/lexicon3.txt || exit 1;
# 
# pushd $dir >&/dev/null
# ln -sf lexicon2.txt lexicon.txt # This is the final lexicon.
# popd >&/dev/null

echo Prepared input dictionary and phone-sets for Switchboard phase 1.

