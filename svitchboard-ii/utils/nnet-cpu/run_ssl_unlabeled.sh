. cmd.sh
. path.sh
set -e # exit on error

# ./steps/decode_fmllr.sh --nj 30 --config conf/decode.config  exp_ssl/tri4a/graph_sw1_tg data/ssl/train_100k_unlabeled exp_ssl/tri4a/decode_unlabeled_sw1_tg

./steps/nnet2/decode.sh --transform-dir exp_ssl/tri4a/decode_unlabeled_sw1_tg --config conf/decode.config --nj 30 exp_ssl/tri4a_nnet/graph_sw1_tg data/ssl/train_100k_unlabeled exp_ssl/tri4a_nnet/decode_unlabeled_sw1_tg