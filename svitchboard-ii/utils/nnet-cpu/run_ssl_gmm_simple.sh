#!/bin/bash

. cmd.sh
. path.sh
set -e # exit on error

# Starting basic training on MFCC features
steps/train_mono.sh --nj 5 --cmd "$train_cmd" \
  data/ssl/train_3k data/lang exp_ssl/mono

steps/align_si.sh --nj 20 --cmd "$train_cmd" \
  data/ssl/train_100k_seed data/lang exp_ssl/mono exp_ssl/mono_ali

steps/train_deltas.sh --cmd "$train_cmd" \
  1500 15000 data/ssl/train_100k_seed data/lang exp_ssl/mono_ali exp_ssl/tri1

for lm_suffix in tg; do
    graph_dir=exp_ssl/tri1/graph_sw1_${lm_suffix}
    $train_cmd $graph_dir/mkgraph.log \
      utils/mkgraph.sh data/ssl/lang_sw1_${lm_suffix} exp_ssl/tri1 $graph_dir
    steps/decode_si.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
      $graph_dir data/eval2000 exp_ssl/tri1/decode_eval2000_sw1_${lm_suffix}
done

steps/align_si.sh --nj 20 --cmd "$train_cmd" \
  data/ssl/train_100k_seed data/lang exp_ssl/tri1 exp_ssl/tri1_ali

steps/train_deltas.sh --cmd "$train_cmd" \
  1500 15000 data/ssl/train_100k_seed data/lang exp_ssl/tri1_ali exp_ssl/tri2


for lm_suffix in tg; do
    graph_dir=exp_ssl/tri2/graph_sw1_${lm_suffix}
    $train_cmd $graph_dir/mkgraph.log \
      utils/mkgraph.sh data/ssl/lang_sw1_${lm_suffix} exp_ssl/tri2 $graph_dir
    steps/decode.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
      $graph_dir data/eval2000 exp_ssl/tri2/decode_eval2000_sw1_${lm_suffix}
done

# From now, we start building a bigger system (on train_100k_nodup, which has
# 110hrs of data). We start with the LDA+MLLT system
steps/align_si.sh --nj 20 --cmd "$train_cmd" \
  data/ssl/train_100k_seed data/lang exp_ssl/tri2 exp_ssl/tri2_ali

# Train tri3b, which is LDA+MLLT, on 100k_nodup data.
steps/train_lda_mllt.sh --cmd "$train_cmd" \
  1500 15000 data/ssl/train_100k_seed data/lang exp_ssl/tri2_ali exp_ssl/tri3b

for lm_suffix in tg; do
    graph_dir=exp_ssl/tri3b/graph_sw1_${lm_suffix}
    $train_cmd $graph_dir/mkgraph.log \
      utils/mkgraph.sh data/ssl/lang_sw1_${lm_suffix} exp_ssl/tri3b $graph_dir
    steps/decode.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
      $graph_dir data/eval2000 exp_ssl/tri3b/decode_eval2000_sw1_${lm_suffix}
done

# Train tri4a, which is LDA+MLLT+SAT, on 100k_nodup data.
# steps/align_fmllr.sh --nj 20 --cmd "$train_cmd" \
#   data/ssl/train_100k_seed data/lang exp_ssl/tri3b exp_ssl/tri3b_ali

steps/train_sat.sh  --cmd "$train_cmd" \
  1500 15000 data/ssl/train_100k_seed data/lang exp_ssl/tri3b_ali \
   exp_ssl/tri4a

for lm_suffix in tg; do
    graph_dir=exp_ssl/tri4a/graph_sw1_${lm_suffix}
    $train_cmd $graph_dir/mkgraph.log \
      utils/mkgraph.sh data/ssl/lang_sw1_${lm_suffix} exp_ssl/tri4a $graph_dir
    steps/decode_fmllr.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
      $graph_dir data/eval2000 exp_ssl/tri4a/decode_eval2000_sw1_${lm_suffix}
done

