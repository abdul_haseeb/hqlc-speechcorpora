# this scripts is used to look up the context from indices
import sys,os,re

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-i", action="store", type="string", dest="input")
parser.add_option("-o", action="store", type="string", dest="output")
parser.add_option("-s", action="store", type="string", dest="sourcefile")
(options,args) = parser.parse_args()

def main():
    
    indices=map(int,open(options.input,'r').readline().split())
    indices.sort()
    
    lines=open(options.sourcefile,'r').readlines()
    fo=open(options.output,'w')
    for i in indices:
        fo.write(lines[i])
                    
    fo.close()
                
if __name__=='__main__':
    main()